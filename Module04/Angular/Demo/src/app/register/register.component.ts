import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  employee: any;
  countries: any;
  departments: any;
  emp: any;

  constructor(private service: EmpService) {

    //Remove the empId attribute from the employee Object    
    this.employee = {
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',

      department: {
        deptId: ''
      }
    }

    //Remove the empId attribute from the employee Object    
    this.emp = {
      empId: ''
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    
    //Fetching all departments
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }


  submit() {
    console.log(this.employee);
    this.service.registerEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
      this.emp = data;
    });

    if (this.emp.empId > 0) {
      alert("Employee Record Inserted Successfully")
    } else {
      alert("Failed to Insert Employee Record");
    }

  }

}
