import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  isUserLoggedIn: boolean;
  loginStatus: any;
  private cartCountSubject = new Subject<number>();
  cartCount$ = this.cartCountSubject.asObservable();

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }

  // Employee CRUD operations

  getAllProducts() {
    return this.http.get('http://localhost:8085/getAllProducts');
  }

  getAllDepartments() {
    return this.http.get('http://localhost:8085/getAllDepartments');
  }

  updateEmployee(employee: any) {
    return this.http.put('http://localhost:8085/updateEmployee', employee);
  }

  getAllEmployees() {
    return this.http.get('http://localhost:8085/getAllEmployees');
  }

  getEmployeeById(empId: any) {
    return this.http.get('http://localhost:8085/getEmployeeById/' + empId).toPromise();
  }

  registerEmployee(employee: any) {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }

  deleteEmployeeById(empId: any) {
    return this.http.delete('http://localhost:8085/deleteEmployeeById/' + empId);
  }

  employeeLogin(emailId: any, password: any) {
    return this.http.get('http://localhost:8085/empLogin/' + emailId + "/" + password).toPromise();
  }

  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  setUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  setUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getLoginStatus(): boolean {
    return this.isUserLoggedIn;
  }

  getUserLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  // Cart functionality
  updateCartCount(count: number) {
    this.cartCountSubject.next(count);
  }
}