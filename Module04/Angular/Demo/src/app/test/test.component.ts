import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent  implements OnInit{
  
 
  id: number;
  name: string;
  age: number;
  hobbies:any;
  address:any;
  person:any={};

  updatePerson(data:any){
    this.person=data;
  }

  constructor() {
    alert('Constructor Invoked...');

    this.id = 101;
    this.name = 'Akhila';
    this.age = 22;
    this.hobbies=['Running','Swimming','music','Movies']
    this.address={streetNo:101,city:'Hyderabad',state:'Telangana'};
  }
  ngOnInit(): void {
    //alert('ngOnInit Invoked...');
  }
}


