import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartProducts: any;
  total: number;
  cartCount: number;
  emailId:any;

  constructor(private router: Router, private empService: EmpService) {
    this.emailId = localStorage.getItem('emailId');
    this.total = 0;
    this.cartCount = 0;
    this.cartProducts = JSON.parse(localStorage.getItem('cartProducts') || '[]');

    this.empService.cartCount$.subscribe(count => {
      this.cartCount = count;
    });

    this.calculateTotal();
  }

  ngOnInit() {
  }

  calculateTotal() {
    this.total = 0;
    this.cartProducts.forEach((element: any) => {
      this.total += element.price;
    });
  }

  purchase() {
    alert('Thank You for Purchasing');
    this.cartProducts = [];
    localStorage.removeItem('cartProducts');
    this.empService.updateCartCount(0);
    this.router.navigate(['products']);
  }

}